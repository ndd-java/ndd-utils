# NDD Commons

NDD Commons provides small and useful utilities.. Released under the [LGPL](https://www.gnu.org/licenses/lgpl.html).

## Usage

TODO

## Setup

Using Maven:

```xml
<dependency>
  <groupId>name.didier.david</groupId>
  <artifactId>ndd-commons</artifactId>
  <version>X.Y.Z</version>
</dependency>
```

## Reports

Maven reports are available at [https://ndd-java.bitbucket.org/ndd-commons/project-reports.html](https://ndd-java.bitbucket.org/ndd-commons/project-reports.html)

## About

Copyright David DIDIER 2014
