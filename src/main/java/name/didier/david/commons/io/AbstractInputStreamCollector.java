package name.didier.david.commons.io;

import static java.nio.charset.StandardCharsets.UTF_8;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An abstract implementation of {@link InputStreamCollector}. Overrides the {@link #onRead(String)} method to do
 * something useful. The default characters set used is {@link StandardCharsets#UTF_8}. Once closed, a collector cannot
 * be reused.
 *
 * @author ddidier
 */
public abstract class AbstractInputStreamCollector
        implements InputStreamCollector {

    /** The associated logger. */
    private final Logger logger;
    /** The encoding used to read the stream. */
    private final Charset charset;
    /** The stream to collect. */
    private final InputStream inputStream;
    /** The stream scanner. */
    private Scanner scanner;

    /**
     * Constructor using {@link StandardCharsets#UTF_8}.
     *
     * @param inputStream the stream to collect.
     */
    protected AbstractInputStreamCollector(InputStream inputStream) {
        this(inputStream, UTF_8);
    }

    /**
     * Constructor using the given encoding.
     *
     * @param inputStream the stream to collect.
     * @param charset the encoding used to read the stream.
     */
    protected AbstractInputStreamCollector(InputStream inputStream, Charset charset) {
        super();
        this.logger = LoggerFactory.getLogger(getClass());
        this.inputStream = checkNotNull(inputStream, "inputStream");
        this.charset = checkNotNull(charset, "charset");
    }

    @Override
    public Charset getCharset() {
        return charset;
    }

    @Override
    public InputStream getInputStream() {
        return inputStream;
    }

    @Override
    public void run() {
        logger.debug("Starting stream collect (charset = {})", getCharset().name());
        scanner = new Scanner(getInputStream(), getCharset().name());

        try {
            while (scanner.hasNext()) {
                onRead(scanner.nextLine());
            }
            logger.debug("Ending stream collect");
        } catch (IllegalStateException e) {
            logger.debug("Stopped stream collect");
        } finally {
            scanner.close();
        }
    }

    @Override
    public void stop() {
        logger.debug("Stopping stream collect");
        scanner.close();
    }

    /**
     * Callback accepting a line read from the stream.
     *
     * @param line a line read from the stream.
     */
    protected abstract void onRead(String line);
}
