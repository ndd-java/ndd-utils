package name.didier.david.commons.io;

import static java.nio.charset.StandardCharsets.UTF_8;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A runnable collecting an {@link InputStream}. The characters set may be specified.
 *
 * @author ddidier
 */
public abstract class AbstractProcessStreamCollector {

    /** The associated logger. */
    private final Logger logger;
    /** The process to collect. */
    private final Process process;
    /** The output stream collector of the process. */
    private final InputStreamCollector outputStreamCollector;
    /** The error stream collector of the process. */
    private final InputStreamCollector errorStreamCollector;

    /**
     * Constructor using {@link StandardCharsets#UTF_8}.
     *
     * @param process the process to collect the streams from.
     */
    protected AbstractProcessStreamCollector(Process process) {
        this(process, UTF_8);
    }

    /**
     * Constructor using the given encoding.
     *
     * @param process the process to collect the streams from.
     * @param charset the encoding used to read the stream.
     */
    protected AbstractProcessStreamCollector(Process process, Charset charset) {
        super();
        checkNotNull(process, "process");
        checkNotNull(charset, "charset");
        this.logger = LoggerFactory.getLogger(getClass());
        this.process = checkNotNull(process, "process");
        this.outputStreamCollector = new OutputStreamCollector(process.getInputStream(), charset);
        this.errorStreamCollector = new ErrorStreamCollector(process.getErrorStream(), charset);
    }

    /**
     * Start in 2 new threads the collection of the process streams.
     */
    public synchronized void start() {
        logger.debug("Starting collection of streams for process {}", process);
        new Thread(outputStreamCollector).start();
        new Thread(errorStreamCollector).start();
        // ExecutorService executorService = Executors.newFixedThreadPool(2);
        // executorService.submit(outputStreamCollector);
        // executorService.submit(errorStreamCollector);
    }

    /**
     * Stop right away the collection of the process streams.
     */
    public synchronized void stop() {
        logger.debug("Stopping collection of streams for process {}", process);
        outputStreamCollector.stop();
        errorStreamCollector.stop();
    }

    /**
     * Stop after the given delay the collection of the process streams. This method will no return before the
     * collection is stopped.
     *
     * @param delay the time from now to delay execution.
     * @param unit the time unit of the delay parameter.
     */
    public void stopBlocking(long delay, TimeUnit unit) {
        logger.debug("Scheduling stopping (blocking) the collection of streams for process {}", process);

        synchronized (this) {
            try {
                wait(unit.toMillis(delay));
            } catch (InterruptedException e) {
                logger.warn("Interrupted while stopping", e);
            }
        }

        stop();
    }

    /**
     * Stop after the given delay the collection of the process streams. This method will return right away.
     *
     * @param delay the time from now to delay execution.
     * @param unit the time unit of the delay parameter.
     */
    public void stopNonBlocking(final long delay, final TimeUnit unit) {
        logger.debug("Scheduling stopping (non blocking) the collection of streams for process {}", process);
        new Thread(new Runnable() {
            @Override
            public void run() {
                stopBlocking(delay, unit);
            }
        }).start();
    }

    /**
     * Callback accepting a line read from the normal stream.
     *
     * @param line a line read from the stream.
     */
    protected abstract void onOutputRead(String line);

    /**
     * Callback accepting a line read from the error stream.
     *
     * @param line a line read from the stream.
     */
    protected abstract void onErrorRead(String line);

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Collect the error stream and forward each line to {@link AbstractProcessStreamCollector#onErrorRead(String)}.
     *
     * @author ddidier
     */
    private final class ErrorStreamCollector
            extends AbstractInputStreamCollector {

        /**
         * Constructor using the given encoding.
         *
         * @param inputStream the stream to collect.
         * @param charset the encoding used to read the stream.
         */
        public ErrorStreamCollector(InputStream inputStream, Charset charset) {
            super(inputStream, charset);
        }

        @Override
        protected void onRead(String line) {
            AbstractProcessStreamCollector.this.onErrorRead(line);
        }
    }

    /**
     * Collect the output stream and forward each line to {@link AbstractProcessStreamCollector#onOutputRead(String)}.
     *
     * @author ddidier
     */
    private final class OutputStreamCollector
            extends AbstractInputStreamCollector {

        /**
         * Constructor using the given encoding.
         *
         * @param inputStream the stream to collect.
         * @param charset the encoding used to read the stream.
         */
        public OutputStreamCollector(InputStream inputStream, Charset charset) {
            super(inputStream, charset);
        }

        @Override
        protected void onRead(String line) {
            AbstractProcessStreamCollector.this.onOutputRead(line);
        }
    }
}
