package name.didier.david.commons.io;

import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * A runnable collecting an {@link InputStream}. The characters set may be specified.
 *
 * @author ddidier
 */
public interface InputStreamCollector
        extends Runnable {

    /**
     * @return the encoding type used to convert bytes from the stream into characters.
     */
    Charset getCharset();

    /**
     * @return the stream to collect.
     */
    InputStream getInputStream();

    /**
     * Stop the collect.
     */
    void stop();
}
