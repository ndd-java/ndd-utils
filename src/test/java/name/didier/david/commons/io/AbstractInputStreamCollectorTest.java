package name.didier.david.commons.io;

import static java.nio.charset.StandardCharsets.UTF_16;
import static java.nio.charset.StandardCharsets.UTF_8;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.mockito.Mock;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgMockitoListener;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public class AbstractInputStreamCollectorTest
        extends AbstractStreamTestBase {

    private static final List<String> LINES = newArrayList("line1", "line2", "line3");

    @Mock
    private InputStream stream;

    // -----------------------------------------------------------------------------------------------------------------

    public void constructor_should_reject_null_stream() {
        try {
            newInputStreamCollector(null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("inputStream");
        }

        try {
            newInputStreamCollector(null, UTF_8);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("inputStream");
        }
    }

    public void constructor_should_reject_null_charset() {
        try {
            newInputStreamCollector(stream, null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("charset");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void getCharset_should_have_a_default() {
        assertThat(newInputStreamCollector(stream).getCharset()).isEqualTo(UTF_8);
    }

    public void getCharset_should_return_the_given_charset() {
        assertThat(newInputStreamCollector(stream, UTF_16).getCharset()).isEqualTo(UTF_16);
    }

    public void getInputStream_should_return_the_given_stream() {
        assertThat(newInputStreamCollector(stream).getInputStream()).isEqualTo(stream);
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void run_should_read_all_lines_from_an_utf8_stream()
            throws IOException {
        InputStreamCollectorImpl collector = newInputStreamCollector(toStream(LINES, UTF_8));
        collector.run();

        assertThat(collector.lines).isEqualTo(LINES);
    }

    public void run_should_read_all_lines_from_an_utf16_stream()
            throws IOException {
        InputStreamCollectorImpl collector = newInputStreamCollector(toStream(LINES, UTF_16), UTF_16);
        collector.run();

        assertThat(collector.lines).isEqualTo(LINES);
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void stop_can_be_called_from_the_same_thread()
            throws IOException {
        InputStreamCollectorImpl1 collector = new InputStreamCollectorImpl1(toStream(LINES, UTF_8), 2);
        collector.run();

        assertThat(collector.lines).isEqualTo(LINES.subList(0, 2));
    }

    public void stop_can_be_called_from_another_thread()
            throws Exception {
        InputStreamCollectorImpl2 collector = new InputStreamCollectorImpl2(toStream(LINES, UTF_8), 2);
        collector.semaphore.acquire();
        new Thread(collector).start();
        collector.semaphore.acquire();
        collector.stop();

        assertThat(collector.lines).isEqualTo(LINES.subList(0, 2));
    }

    // -----------------------------------------------------------------------------------------------------------------

    private InputStreamCollectorImpl newInputStreamCollector(InputStream inputStream) {
        return new InputStreamCollectorImpl(inputStream);
    }

    private InputStreamCollectorImpl newInputStreamCollector(InputStream inputStream, Charset charset) {
        return new InputStreamCollectorImpl(inputStream, charset);
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class InputStreamCollectorImpl
            extends AbstractInputStreamCollector {

        protected final List<String> lines = new ArrayList<>();
        protected final int maximumLinesNumber;

        public InputStreamCollectorImpl(InputStream inputStream) {
            super(inputStream);
            this.maximumLinesNumber = Integer.MAX_VALUE;
        }

        public InputStreamCollectorImpl(InputStream inputStream, Charset charset) {
            super(inputStream, charset);
            this.maximumLinesNumber = Integer.MAX_VALUE;
        }

        public InputStreamCollectorImpl(InputStream inputStream, int maximumLinesNumber) {
            super(inputStream);
            this.maximumLinesNumber = maximumLinesNumber;
        }

        public InputStreamCollectorImpl(InputStream inputStream, Charset charset, int maximumLinesNumber) {
            super(inputStream, charset);
            this.maximumLinesNumber = maximumLinesNumber;
        }

        @Override
        protected void onRead(String line) {
            lines.add(line);
        }
    }

    private static class InputStreamCollectorImpl1
            extends InputStreamCollectorImpl {

        public InputStreamCollectorImpl1(InputStream inputStream, int maximumLinesNumber) {
            super(inputStream, maximumLinesNumber);
        }

        @Override
        protected void onRead(String line) {
            super.onRead(line);

            if (lines.size() == maximumLinesNumber) {
                stop();
            }
        }
    }

    private static class InputStreamCollectorImpl2
            extends InputStreamCollectorImpl {

        protected Semaphore semaphore = new Semaphore(1, true);

        public InputStreamCollectorImpl2(InputStream inputStream, int maximumLinesNumber) {
            super(inputStream, maximumLinesNumber);
        }

        @Override
        protected void onRead(String line) {
            super.onRead(line);

            if (lines.size() == maximumLinesNumber) {
                semaphore.release();

                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    fail("Error while acquiring semaphore", e);
                }
            }
        }
    }
}
