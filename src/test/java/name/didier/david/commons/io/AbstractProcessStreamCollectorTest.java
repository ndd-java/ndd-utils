package name.didier.david.commons.io;

import static java.nio.charset.StandardCharsets.UTF_16;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.when;

import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.test4j.testng.TestNgGroups.SLOW;
import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.mockito.Mock;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgMockitoListener;

@Test(groups = { UNIT, SLOW })
@Listeners(TestNgMockitoListener.class)
public class AbstractProcessStreamCollectorTest
        extends AbstractStreamTestBase {

    private static final List<String> OUTPUT_LINES = newArrayList("output1", "output2", "output3");
    private static final List<String> ERROR_LINES = newArrayList("error1", "error2", "error3");
    private static final int COLLECTOR_TIMEOUT = 10;

    @Mock
    private Process process;

    // -----------------------------------------------------------------------------------------------------------------

    @SuppressWarnings("unused")
    public void constructor_should_reject_null_process() {
        try {
            new ProcessStreamCollectorImpl(null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("process");
        }

        try {
            new ProcessStreamCollectorImpl(null, UTF_8);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("process");
        }
    }

    @SuppressWarnings("unused")
    public void constructor_should_reject_null_charset() {
        try {
            new ProcessStreamCollectorImpl(process, null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("charset");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void run_should_read_all_lines_from_an_utf8_stream()
            throws Exception {
        ProcessStreamCollectorImpl collector = newMockedCollector(UTF_8);
        collector.start();
        collector.stopBlocking(COLLECTOR_TIMEOUT, MILLISECONDS);

        assertThat(collector.outputLines).containsExactlyElementsOf(OUTPUT_LINES);
        assertThat(collector.errorLines).containsExactlyElementsOf(ERROR_LINES);
    }

    public void run_should_read_all_lines_from_an_utf16_stream()
            throws Exception {
        ProcessStreamCollectorImpl collector = newMockedCollector(UTF_16);
        collector.start();
        collector.stopBlocking(COLLECTOR_TIMEOUT, MILLISECONDS);

        assertThat(collector.outputLines).containsExactlyElementsOf(OUTPUT_LINES);
        assertThat(collector.errorLines).containsExactlyElementsOf(ERROR_LINES);
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void run_should_read_lines_from_a_stream_until_stop_is_called()
            throws Exception {
        ProcessStreamCollectorImpl collector = newMockedCollector(UTF_8, 2);

        collector.semaphore.acquire(2);
        collector.start();
        collector.semaphore.acquire(2);
        collector.stop();

        assertThat(collector.outputLines).isEqualTo(OUTPUT_LINES.subList(0, 2));
        assertThat(collector.errorLines).isEqualTo(ERROR_LINES.subList(0, 2));
    }

    public void run_should_read_lines_from_a_stream_until_stopBlocking_is_called()
            throws Exception {
        ProcessStreamCollectorImpl collector = newMockedCollector(UTF_8, 2);

        collector.semaphore.acquire(2);
        collector.start();
        collector.semaphore.acquire(2);
        collector.stopBlocking(COLLECTOR_TIMEOUT, MILLISECONDS);

        assertThat(collector.outputLines).isEqualTo(OUTPUT_LINES.subList(0, 2));
        assertThat(collector.errorLines).isEqualTo(ERROR_LINES.subList(0, 2));
    }

    public void run_should_read_lines_from_a_stream_until_stopNonBlocking_is_called()
            throws Exception {
        ProcessStreamCollectorImpl collector = newMockedCollector(UTF_8, 2);

        collector.semaphore.acquire(2);
        collector.start();
        collector.semaphore.acquire(2);
        collector.stopNonBlocking(COLLECTOR_TIMEOUT, MILLISECONDS);
        waitForTimeout();

        assertThat(collector.outputLines).isEqualTo(OUTPUT_LINES.subList(0, 2));
        assertThat(collector.errorLines).isEqualTo(ERROR_LINES.subList(0, 2));
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void test_stop()
            throws Exception {
        ProcessStreamCollectorImpl collector = newMockedCollector(UTF_8);
        collector.start();
        waitForTimeout();
        collector.stop();

        assertThat(collector.outputLines).containsExactlyElementsOf(OUTPUT_LINES);
        assertThat(collector.errorLines).containsExactlyElementsOf(ERROR_LINES);
    }

    public void test_stopBlocking()
            throws Exception {
        ProcessStreamCollectorImpl collector = newMockedCollector(UTF_8);
        collector.start();
        collector.stopBlocking(COLLECTOR_TIMEOUT, MILLISECONDS);

        assertThat(collector.outputLines).containsExactlyElementsOf(OUTPUT_LINES);
        assertThat(collector.errorLines).containsExactlyElementsOf(ERROR_LINES);
    }

    public void test_stopNonBlocking()
            throws Exception {
        ProcessStreamCollectorImpl collector = newMockedCollector(UTF_8);
        collector.start();
        collector.stopNonBlocking(COLLECTOR_TIMEOUT, MILLISECONDS);
        waitForTimeout();

        assertThat(collector.outputLines).containsExactlyElementsOf(OUTPUT_LINES);
        assertThat(collector.errorLines).containsExactlyElementsOf(ERROR_LINES);
    }

    // -----------------------------------------------------------------------------------------------------------------

    private ProcessStreamCollectorImpl newMockedCollector(Charset charset) {
        when(process.getInputStream()).thenReturn(toStream(OUTPUT_LINES, charset));
        when(process.getErrorStream()).thenReturn(toStream(ERROR_LINES, charset));
        return new ProcessStreamCollectorImpl(process, charset);
    }

    private ProcessStreamCollectorImpl newMockedCollector(Charset charset, int maximumLinesNumber) {
        when(process.getInputStream()).thenReturn(toStream(OUTPUT_LINES, charset));
        when(process.getErrorStream()).thenReturn(toStream(ERROR_LINES, charset));
        return new ProcessStreamCollectorImpl(process, charset, maximumLinesNumber);
    }

    private void waitForTimeout() {
        synchronized (this) {
            try {
                wait(COLLECTOR_TIMEOUT);
            } catch (InterruptedException e) {
                fail("Error while waiting", e);
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class ProcessStreamCollectorImpl
            extends AbstractProcessStreamCollector {

        protected final int maximumLinesNumber;
        protected Semaphore semaphore = new Semaphore(2, true);
        protected List<String> outputLines = new ArrayList<>();
        protected List<String> errorLines = new ArrayList<>();

        protected ProcessStreamCollectorImpl(Process process) {
            super(process);
            this.maximumLinesNumber = Integer.MAX_VALUE;
        }

        protected ProcessStreamCollectorImpl(Process process, Charset charset) {
            super(process, charset);
            this.maximumLinesNumber = Integer.MAX_VALUE;
        }

        protected ProcessStreamCollectorImpl(Process process, int maximumLinesNumber) {
            super(process);
            this.maximumLinesNumber = maximumLinesNumber;
        }

        protected ProcessStreamCollectorImpl(Process process, Charset charset, int maximumLinesNumber) {
            super(process, charset);
            this.maximumLinesNumber = maximumLinesNumber;
        }

        @Override
        protected void onOutputRead(String line) {
            outputLines.add(line);
            if (outputLines.size() == maximumLinesNumber) {
                release();
            }
        }

        @Override
        protected void onErrorRead(String line) {
            errorLines.add(line);
            if (errorLines.size() == maximumLinesNumber) {
                release();
            }
        }

        private void release() {
            semaphore.release();
            try {
                synchronized (this) {
                    // otherwise there may be a deadlock (fairness?)
                    wait(COLLECTOR_TIMEOUT);
                }
                semaphore.acquire();
            } catch (InterruptedException e) {
                fail("Error while acquiring semaphore", e);
            }
        }
    }
}
