package name.didier.david.commons.io;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;

import com.google.common.base.Joiner;

public abstract class AbstractStreamTestBase {

    protected static InputStream toStream(List<String> lines, Charset charset) {
        return new ByteArrayInputStream(Joiner.on("\n").join(lines).getBytes(charset));
    }
}
